package com.slowdraft.slowdraft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlowdraftApplication {

	public static void main(String[] args) {
		SpringApplication.run(SlowdraftApplication.class, args);
	}

}
