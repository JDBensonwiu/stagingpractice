package com.slowdraft.slowdraft;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.slowdraft.slowdraft.model.User;
import com.slowdraft.slowdraft.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	final UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}
	
	
	@GetMapping("/all")
	public ResponseEntity<List<User>> getAllUsers () {
	List<User> users = userService.returnAllUsers();
	return new ResponseEntity<>(users, HttpStatus.OK);
	}
	
	
	@GetMapping("/find/{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") Long id) {
	User currentUser = userService.findUserById(id);
	return new ResponseEntity<>(currentUser, HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public  ResponseEntity<User> addUser(@RequestBody User user){
		User currentUser = userService.updateUser(user);
		return new ResponseEntity<>(currentUser, HttpStatus.CREATED);		
	}
	
	@PutMapping("/update")
	public  ResponseEntity<User> updateUser(@RequestBody User user){
		User updatedUser = userService.addUser(user);
		return new ResponseEntity<>(updatedUser, HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public  ResponseEntity<?> deleteUser(@PathVariable("id") Long id){
		userService.deleteUser(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
	}
